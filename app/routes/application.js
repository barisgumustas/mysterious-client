import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel: function(transition) {
    if (!this.get('auth.user')) {
      this.transitionTo('index');
    }
  },

  actions: {
    login: function () {
      var self = this;
      this.get('store').find('user', 'me').then(function (user) {
        self.set('auth.user', user);
        self.transitionTo('lists');
      }, function (response) {
        self.set('auth.errors', response.errors);
      });
    },

    closeFlash: function () {
      this.set('flash.show', false);
    }
  }
});
