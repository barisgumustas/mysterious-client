import Ember from 'ember';

export default Ember.Service.extend({
  type:     null,
  message:  null,
  show:     false,
  timeout:  null,

  typeClass: function () {
    return 'flash-' + this.get('type');
  }.property('type'),

  display: function (type, message) {
    this.setProperties({
      type:     type,
      message:  message,
      show:     true
    });
  },

  autoHide: function () {
    if (this.get('show')) {
      Ember.run.cancel(this.get('timeout'));

      var timeout = Ember.run.later(this, function() {
        this.set('show', false);
      }, 2500);

      this.set('timeout', timeout);
    }
  }.observes('show')
});
