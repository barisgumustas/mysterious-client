import Ember from 'ember';

export default Ember.Service.extend({
  username: null,
  password: null,
  user:     null,

  authenticationToken: function () {
    return btoa(this.get('email') + ':' + this.get('password'));
  }.property('username', 'password'),

  ajaxPrefilter: function () {
    var self = this;
    Ember.$.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
      return jqXHR.setRequestHeader('Authorization', 'Basic ' + self.get('authenticationToken'));
    });
  }.on('init'),
});
