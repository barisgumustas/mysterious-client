import Ember from 'ember';

export default Ember.Component.extend({
  classNames: ['listItem-list', 'u-relative'],
  tagName: 'li',
  actions: {
    destroy: function () {
      var self  = this,
          list  = this.get('list.store').peekRecord('list', this.get('list.id')),
          flash = self.get('flash');

      console.log(list === this.get('list'));

      self.set('isBusy', true);
      list.destroyRecord().then(function () {
        flash.display('success', 'List destroyed.');
        self.get('list').destroy();
        self.sendAction('destroyedAction', list);
      }, function (response) {
        // self.set('isBusy', false);
        list.rollback();
        flash.display('fail', response.errors.join(" "));
      });
    },
  }
});
