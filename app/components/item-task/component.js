import Ember from 'ember';

export default Ember.Component.extend({
  tagName:    'li',
  classNames: ['listItem-task'],

  actions: {
    toggle: function () {
      var self = this;

      self.set('isBusy', true);
      self.get('task').toggleProperty('isDone');

      self.get('task').save().then(function () {
        self.get('flash').display('success', 'Task saved.');
      }, function (response) {
        self.get('flash').display('fail', response.errors.join(" "));
        self.get('task').rollback();
      }).finally(function () {
        self.set('isBusy', false);
      });
    },
    destroy: function (task) {
      var self  = this,
          flash = self.get('flash');

      self.set('isBusy', true);
      task.destroyRecord().then(function () {
        flash.display('success', 'Task destroyed.');
      }, function (response) {
        task.rollback();
        flash.display('fail', response.errors.join(" "));
        self.set('isBusy', false);
      });
    }
  }
});
