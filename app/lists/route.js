import Ember from 'ember';

export default Ember.Route.extend({
  model: function () {
    return this.get('store').findAll('list');
  },
  actions: {
    listDestroyed: function (list) {
      if (list.get('id') === this.paramsFor('lists.show').id) {
        this.transitionTo('lists');
      }
    }
  }
});
