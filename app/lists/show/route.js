import Ember from 'ember';

export default Ember.Route.extend({
  model: function (params) {
    return this.get('store').find('list', params.id);
  },
  setupController: function (controller, model) {
    controller.set('description', '');
    return this._super(controller, model);
  }
});
