import Ember from 'ember';

export default Ember.Controller.extend({
  list: Ember.computed.alias('model'),

  actions: {
    create: function () {
      var self = this;
      var task = this.get('store').createRecord('task', {
        list:         this.get('list'),
        description:  this.get('description')
      });

      self.set('isBusy', true);
      task.save().finally(function () {
        self.set('isBusy', false);
      }).then(function () {
        self.get('flash').display('success', 'Task created.');
        self.set('description', '');
      }, function (response) {
        self.get('flash').display('fail', response.errors.join(" "));
        task.rollback();
      });
    }
  }
});
