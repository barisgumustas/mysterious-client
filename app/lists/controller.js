import Ember from 'ember';

export default Ember.Controller.extend({
  lists: Ember.computed.alias('model'),
  actions: {
    create: function () {
      var self = this;
      var list = this.get('store').createRecord('list', {
        title: this.get('title')
      });

      self.set('isBusy', true);
      list.save().finally(function () {
        self.set('isBusy', false);
      }).then(function () {
        self.get('flash').display('success', 'List created.');
        self.set('title', '');
      }, function (response) {
        self.get('flash').display('fail', response.errors.join(" "));
        task.rollback();
      });
    }
  }
});
