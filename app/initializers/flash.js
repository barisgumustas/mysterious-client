export function initialize(container, application) {
  application.inject('route',       'flash', 'service:flash');
  application.inject('controller',  'flash', 'service:flash');
  application.inject('component',   'flash', 'service:flash');
}

export default {
  name: 'flash',
  initialize
};
