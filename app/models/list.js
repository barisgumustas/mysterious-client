import DS from 'ember-data';

export default DS.Model.extend({
  title:  DS.attr('string'),
  user:   DS.belongsTo('user', { async: false }),
  tasks:  DS.hasMany('task', { async: true })
});
