 // var proxy = require('http-proxy')

module.exports = function(app) {

  var httpProxy = require('http-proxy').createProxyServer({ xfwd: true });

  app.all("/api/*", function(req, res){
    httpProxy.web(req, res, { target: 'http://localhost:3000' });
  });
};
